#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <utility>
#include <cmath>
#include <cstdio>
#include <algorithm>

using namespace std;

int main(int argc, char ** argv)
{
    if (argc < 3)
    {
        cout << "./<program><neighbours><path_to_file>" << endl;
    }
    int n = stoi(argv[1]);
    vector<string> dataset;
    map<string, int> wordmap;
    ifstream in(argv[2]);
    if (in.is_open())
    {
        string word;
        while (getline(in, word))
        {
            dataset.push_back(word);
            wordmap[word]++;
        }
        in.close();
    }
    else
    {
        cout << "Cannot open file." << endl;
        return -1;
    }

    // filtrace
    cout << "wordmap size " << wordmap.size() << endl;
    for (auto it = wordmap.begin(); it != wordmap.end(); it++)
    {
        while (it->second < 10)
        {
            it = wordmap.erase(it);
        }
    }
    cout << "wordmap(reduced) size " << wordmap.size() << endl;

    int dss = 0;
    for (auto it : wordmap)
    {
        dss += it.second;
    }

    // vytvoření párů
    map<pair<string, string>, int> pairs;

    for (size_t wpos = 0; wpos < dataset.size(); wpos++)
    {
        if (n > 0)
        {
            for (int cyc = wpos - n; cyc < (int)wpos + n; cyc++)
            {
                if (cyc < 0 || cyc >= (int)dataset.size() || cyc == (int)wpos)
                {
                    continue;
                }

                if (wordmap[dataset[wpos]] >= 10 && wordmap[dataset[cyc]] >= 10)
                {
                    pair<string, string> p{dataset[wpos], dataset[cyc]};
                    pairs[p]++;
                }
            }
        }
        else
        {
            if (wpos == 0)
            {
                continue;
            }

            if (wordmap[dataset[wpos-1]] >= 10 && wordmap[dataset[wpos]] >= 10)
            {
                pair<string, string> p{dataset[wpos-1], dataset[wpos]};
                pairs[p]++;
            }
        }
    }

    int paircnt = 0;
    map<pair<string, string>, double> prob;

    for (const auto& el : pairs)
    {
        paircnt += el.second;
    }

    for (const auto& el : pairs)
    {
        double ww = (double)el.second / (double)paircnt;
//        printf("%s %s %d %d\r\n", el.first.first.c_str(), el.first.second.c_str(), el.second, paircnt);
        double w1 = (double)wordmap[el.first.first] / (double)dss;
        double w2 = (double)wordmap[el.first.second] / (double)dss;
//        printf("ww: %f, w1: %f, w2: %f\r\n", ww, w1, w2);
        prob[el.first] = log2(ww / (w1 * w2));
    }

    vector<pair<pair<string, string>, double>> sortedpairs;

    for (const auto& el : prob)
    {
        sortedpairs.push_back(el);
    }

    sort(sortedpairs.begin(), sortedpairs.end(), [](pair<pair<string, string>, double> p1, pair<pair<string, string>, double> p2){
        return p1.second > p2.second;
    });

    ofstream out("en_50.csv");

    if (out.is_open())
    {
        for (const auto& el : sortedpairs)
        {
//            printf("%s %s, %f\r\n", el.first.first.c_str(), el.first.second.c_str(), el.second);
            out << "\"" << el.first.first.c_str() << "\" \"" << el.first.second.c_str() << "\"," << el.second << endl;
        }
        out.close();
    }

    return 0;
}
